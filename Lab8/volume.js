// TODO: Add the calculateVolume function that takes radius, height, as parameters and returns the calculated volume as a number
function calculateVolume(radius, height, scoops){
// Calculate volume when the button Calculate is clicked
	scoop = scoops*((4/3)*(Math.PI*(Math.pow(radius,3))));
	volume =(height/3)*(Math.PI*(Math.pow(radius,2)));
	number = scoop + volume;
	return number;
}
const button = document.querySelector("button");
button.addEventListener("click", function() {
  let radius = document.getElementById("radius").valueAsNumber;
  let height = document.getElementById("height").valueAsNumber;
  let scoops = document.getElementById("scoops").valueAsNumber;
  
if (radius <1 || height <1 || scoops < 1) {
    alert("None of our values should be less than 1!");
  }  
	else if (!(Number.isInteger(scoops))) {
	alert("We can only give out full scoops!");
  }
  else {
    alert("Cone volume is " + calculateVolume(radius, height, scoops).toPrecision(3) + " cm^3" );
  }
});
